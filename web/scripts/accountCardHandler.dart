import 'modalTools.dart';
import 'definitions.dart';
import 'generatorHandler.dart' as generator;
import 'localize.dart';
import 'activeHandler.dart';

import 'dart:html';
import 'dart:convert';
import 'dart:math';

Random randomFactory = new Random.secure(); 
List passwordLengths = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];

void addWebsiteCard(Map accountObject) {
  Element passwordsPage = document.getElementById('passwordsPage');
  Element websiteCard = new DivElement();
  Element titleInCard = new HeadingElement.h2();
  Element subtitleInCard = new ParagraphElement();

  titleInCard.text = accountObject['service'];
  subtitleInCard.text = accountObject['username'];

  websiteCard.children.add(titleInCard);
  websiteCard.children.add(subtitleInCard);
  websiteCard.classes.add('websiteCard');
  passwordsPage.children.add(websiteCard);

  websiteCard.onClick.listen(
    (event) => openModal(accountObject)
  );
}

void initCards() async {
  Element passwordsPage = await document.getElementById('passwordsPage');
  passwordsPage.innerHtml = await '';

  Element pageTitle = new HeadingElement.h1();
  await pageTitle.classes.add('local_passwordsTitle');
  await passwordsPage.children.add(pageTitle);

  List accountList = await readAccountList();
  for (var i = 0; i < accountList.length; i++) {
    var accountObject = await json.decode(accountList[i]);
    var passwordText = await new ParagraphElement();
    passwordText.text = await accountObject['password'];
    await addWebsiteCard(accountObject);
  }

  Element passwordsPageAddAccountButton = new AnchorElement();
  await passwordsPageAddAccountButton.setAttribute('href', '#');
  passwordsPageAddAccountButton.id = 'passwordsPageAddAccountButton';
  await passwordsPageAddAccountButton.classes.add('local_passwordsPageAddAccountButton');
  await passwordsPage.children.add(passwordsPageAddAccountButton);

  document.getElementById('passwordsPageAddAccountButton').onClick.listen(
    (event) => setActive('addPage')
  );

  await initLocalize();
}

void initAddAccountPage() {
  void doTheThing() {
    InputElement servicebox = (document.getElementById('servicebox') as InputElement);
    InputElement usernamebox = (document.getElementById('usernamebox') as InputElement);
    storeAccount(servicebox.value, usernamebox.value, generator.secureStringDart(passwordLengths[randomFactory.nextInt(passwordLengths.length)], r"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#_=+$^"));
    setActive('passwordsPage');
    servicebox.value = '';
    usernamebox.value = '';
  }
  document.getElementById('addAccountButton').onClick.listen(
    (event) => doTheThing()
  );
}