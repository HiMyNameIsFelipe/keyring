import 'dart:html';
import '../localization/index.dart';

String region = window.localStorage['language'];

void initLocalize() async {
  void theThing(String k) async {
    var fullClassName = await 'local_' + k;
    var elementsToBeLocalized = document.getElementsByClassName(fullClassName);

    void updateTextForSpecificElement(Element element, String text) {
      if (element is TitleElement || 
          element is DivElement || 
          element is HeadingElement || 
          element is ParagraphElement || 
          element is SpanElement ||
          element is AnchorElement ) {
        element.text = text;
      }

      if (element is InputElement) {
        if (element.getAttribute('type') == "text") {
          (element as InputElement).placeholder = text;
        }

        if (element.getAttribute('type') == "password") {
          (element as InputElement).placeholder = text;
        }

        if (element.getAttribute('type') == "button") {
          (element as InputElement).value = text;
        }
      }

    }

    elementsToBeLocalized.forEach((element) => updateTextForSpecificElement(element, localization[region][k]));
  };

  localization[region].forEach((k,v) => theThing(k)); 
}