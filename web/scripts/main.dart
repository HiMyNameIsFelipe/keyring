import 'definitions.dart';
import 'sidebarHandler.dart';
import 'activeHandler.dart';
import 'generatorHandler.dart' as generator;
import 'accountCardHandler.dart';
import 'modalTools.dart';
import 'clipboard.dart';
import 'localize.dart';
import 'settingsHandler.dart';

import 'dart:html';

main() {
  // window.localStorage.clear();
  initlocalstorage();
  setActive('passwordsPage');

  initSidebar();
  generator.init();

  // addWebsiteCard('Test', 'Subtest');

  initLocalize();

  if (window.localStorage['encrypted'] == 'true') {
    getPassword();
  }

  initCards();
  initAddAccountPage();
  initSettingsHandler();

  document.getElementById('blocker').onClick.listen(
    (event) => closeModal()
  );
  document.getElementById('modalCloseButton').onClick.listen(
    (event) => closeModal()
  );
  document.getElementById('passwordField').onClick.listen(
    (event) => copyAndCloseModal()
  );
}

void copyAndCloseModal() {
  copyToClipboard(document.getElementById('passwordField').text);
  closeModal();
}

void initlocalstorage() {
  if(window.localStorage['isSetUp'] != "true") {
    window.localStorage['language'] = "en_us";
    window.localStorage['encrypted'] = "false";
    window.localStorage['iv'] = generator.secureStringDart(8, r"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#_=+$^");
    writeAccountList([]);
    writeEncryptionTest();
    window.localStorage['isSetUp'] = "true";
  }
}